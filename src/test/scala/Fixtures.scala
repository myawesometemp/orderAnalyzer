package com.walmart

import models.*

import java.time.LocalDate

object Fixtures {
  val recentProduct1 = Product("product1", LocalDate.now.minusDays(1))
  val recentProduct2 = Product("product12", LocalDate.now.minusDays(1))

  val fourMonthProduct = Product("product2", LocalDate.now.minusMonths(4))

  val sevenMonthProduct = Product("product3", LocalDate.now.minusMonths(7))

  val moreThenYearProduct = Product("product4", LocalDate.now.minusYears(2))

  val products = List(recentProduct1, recentProduct2, fourMonthProduct, sevenMonthProduct, moreThenYearProduct)

}
