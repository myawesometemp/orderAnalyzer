package com.walmart

import Fixtures.*
import models.Order

import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import java.time.{LocalDate, LocalDateTime}

class OrderAnalizerSpec extends AnyWordSpec with Matchers {
  val start: LocalDateTime = LocalDateTime.now().minusYears(1)
  val end: LocalDateTime = LocalDateTime.now().minusMonths(1)

  val emptyResult = Map(
    "1-3 months" -> 0,
    "4-6 months" -> 0,
    "7-12 months" -> 0,
    ">12 months" -> 0
  )


  "AnilizeOrder" should {
    "filter order by interval" in {
      val todayOrder = Order(1, LocalDate.now, List(moreThenYearProduct.toItem))
      val oldOrder = Order(1, LocalDate.now.minusYears(10), List(moreThenYearProduct.toItem))

      OrderAnalyzer.analyzeOrders(List(todayOrder, oldOrder), start, end) shouldBe emptyResult
    }
    "group correct" in {
      val order1 = Order(1, LocalDate.now.minusMonths(2), products.map(_.toItem))
      val order2 = Order(2, LocalDate.now.minusMonths(2), List(moreThenYearProduct.toItem, recentProduct2.toItem))

      val expected = Map(
        "1-3 months" -> 2,
        "4-6 months" -> 1,
        "7-12 months" -> 1,
        ">12 months" -> 2
      )

      OrderAnalyzer.analyzeOrders(List(order1, order2), start, end) shouldBe expected
    }
  }

}
