package com.walmart.models

import com.walmart.models.Item

import java.time.{LocalDate, LocalDateTime}
import java.util.UUID


case class Order(id: Long, datePlaced: LocalDate, items: List[Item], customerName: String = "Customer1", shippingAddress: String = "Street", grandTotal: Double = 1.2)
