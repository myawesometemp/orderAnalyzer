package com.walmart.models

import java.time.{LocalDate, LocalDateTime, Period}

case class Product(name: String,creationDate: LocalDate,category: String = "sport", weight: Double = 1.0, price: Double = 1.0) {
  val monthAge: Long = Period.between(creationDate, LocalDate.now).toTotalMonths

  lazy val toItem = Item(this)

  def inInterval(collectInterval: CollectInterval): Boolean = creationDate.isBefore(collectInterval.start) && collectInterval.endMaybe.forall(creationDate.isAfter)
}

