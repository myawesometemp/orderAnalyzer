package com.walmart.models

case class Item(product: Product, cost: Double = 0.1, shippingFee: Double = 0.1, taxAmount: Double = 0.1)