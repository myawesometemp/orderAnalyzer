package com.walmart
package models

import java.time.LocalDate

case class CollectInterval(start: LocalDate, endMaybe: Option[LocalDate] = None)

case object CollectInterval {
  def apply(startMonth: Int, endMonthMaybe: Option[Int]): CollectInterval = CollectInterval(
    start = LocalDate.now().minusMonths(startMonth),
    endMaybe = endMonthMaybe.map(LocalDate.now().minusMonths(_))
  )

  def apply(startMonth: Int): CollectInterval = CollectInterval(start = LocalDate.now().minusMonths(startMonth))

  def validate(intervals: List[CollectInterval]): Either[String, Unit] = ???
}
