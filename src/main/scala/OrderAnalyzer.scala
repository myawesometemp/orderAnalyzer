package com.walmart

import models.*

import java.time.{LocalDate, LocalDateTime, Period}


object OrderAnalyzer {
  lazy val orders = ???

  def main(args: Array[String]): Unit = {
    val dateFormat = java.time.format.DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
    val startInterval = LocalDateTime.parse(args(0), dateFormat)
    val endInterval = LocalDateTime.parse(args(1), dateFormat)

    val result = analyzeOrders(orders, startInterval, endInterval)

    result.foreach { case (interval, count) =>
      println(s"$interval: $count orders")
    }
  }

  def analyzeOrders(orders: List[Order], startInterval: LocalDateTime, endInterval: LocalDateTime): Map[String, Int] = {
    val intervals: List[(String, CollectInterval)] = List(
      ("1-3 months", CollectInterval(0, Some(3))),
      ("4-6 months", CollectInterval(3, Some(6))),
      ("7-12 months", CollectInterval(6, Some(12))),
      (">12 months", CollectInterval(12))
    )


    val productWithOrder: List[(Order, Product)] = for {
      order <- orders if order.datePlaced.atStartOfDay.isAfter(startInterval) && order.datePlaced.atStartOfDay.isBefore(endInterval)
      item <- order.items
    } yield order -> item.product

    intervals.map { case (intervalName, collectInterval) =>
      val intervalSize = productWithOrder.withFilter { case (order, product) =>
        product.inInterval(collectInterval)
      }.map { case (order, product) =>
        order.id -> product
      }.toMap.size

      intervalName -> intervalSize
    }.toMap
  }
}
